# pulls contrib

This is a Go "script" to convert all PRs like `#100` to `[#100](https://github.com/go-gitea/gitea/pull/100)`

### Usage

From the base directory
```
go run contrib/pulls/pulls.go --release 1.11.4
```
